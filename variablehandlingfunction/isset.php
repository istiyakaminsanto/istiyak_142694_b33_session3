<?php

$a = array ('test' => 1, 'hello' => NULL, 'pie' => array('a' => 'apple','b' =>'mango'));

var_dump(isset($a['test']));    echo "<br>";        // TRUE
var_dump(isset($a['foo']));     echo "<br>";        // FALSE
var_dump(isset($a['hello']));   echo "<br>";        // FALSE

// The key 'hello' equals NULL so is considered unset
// If you want to check for NULL key values then try: 
var_dump(array_key_exists('hello', $a)); // TRUE

// Checking deeper array values
var_dump(isset($a['pie']['a']));      echo "<br>";  // TRUE
var_dump(isset($a['pie']['b']));      echo "<br>";  // FALSE
var_dump(isset($a['cake']['a']['b']));echo "<br>";  // FALSE

?>
